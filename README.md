# Installation
- clone the repo
- `composer install`
- copy `public_html/config.json.template` to `public_html/config.json`
- change `public_html/config.json` to reflect your Wikibase setup
- create and approve an OAuth consumer on your Wikibase setup (eg https://meta.wikimedia.org/wiki/Special:OAuthConsumerRegistration)
- create an `oauth.ini` file:
```
[settings]
agent = tabernacle
consumerKey = SOMETHING
consumerSecret = SOMETHING_ELSE
```

Your version of Tabernacle should now be available under `public_html/index.html`.
