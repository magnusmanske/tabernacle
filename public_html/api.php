<?php
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require '../vendor/autoload.php';

$config = json_decode(file_get_contents('config.json'));
$oauth_url = $config->wd->oauth ?? '' ;
$widar = new Toolforge\Widar ( 'tabernacle' , $oauth_url ) ;
$widar->attempt_verification_auto_forward ( $config->tool_url ) ;
$widar->authorization_callback = $config->tool_url.'api.php' ;
if ( $widar->render_reponse ( true ) ) exit ( 0 ) ;
print json_encode ( ['data'=>'ERROR'] ) ;

?>